#svtplay downloader#

This script makes it easier to download shows from [svtplay.se](http://svtplay.se) for later viewing. If you have a slow connection or if you are going somewhere without internet connection this is a nice solution.

Use this for your own convenience (not to upload shows to other sites), [svtplay.se](http://svtplay.se/) is a great initiative and this is just an enhancement to that experience for those who need it.

Downloaded shows are in **.mp4** format and can be transferred to iPhone and iPad directly.

**This script does not currently support live broadcasts**

##Requirements##

* python 2.5+
* [rtmpdump](http://rtmpdump.mplayerhq.hu/)

##Installation##

* install [rtmpdump](http://rtmpdump.mplayerhq.hu/)
* download or clone this repository

##Usage##

1. open a terminal, change to download directory and type `svt-download.py urltoshow path` (path is optional), filename will be url format.
2. follow the instructions in the prompt
3. enjoy the show
