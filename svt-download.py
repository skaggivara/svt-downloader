#!/usr/bin/env python
import sys, re, json, subprocess, os
from urllib2 import urlopen

API_URL = "http://www.svtplay.se/video/%s?type=embed&position=&output=json"

def file_type(url):
    return url[len(url)-3:len(url)]

def choose_bitrate(videos):
    
    print "Available bitrates: (%s)" % ','.join(str(x['bitrate']) for x in videos)
    
    bitrate = raw_input("Choose bitrate: ")
    
    for i in range(0, len(videos)):
        if str(videos[i]['bitrate']) == bitrate:
            return videos[i]
    
    print "Choosen bitrate (%s) not found" % bitrate
    return None

def download(url, path=''):
    
    if len(path) == 0:
        path = "%s/" % os.getcwd()
    
    try:
        videoid = parse_url(url)
    except Exception, e:
        sys.exit("Invalid url")
    
    options = fetch_options(videoid)
    
    if len(options['videos']) > 0:
        
        # Check for rtmpdump first
        try:
            subprocess.call(['rtmpdump', '-h'], stdout=(file(os.path.devnull, 'w')), stderr=subprocess.STDOUT)
        except (OSError, IOError):
            print 'ERROR: RTMP download detected but "rtmpdump" could not be run'
            return False
        
        # choose bitrate
        video = None
        while video is None:
            video = choose_bitrate(options['videos'])
        
        filename = '%s%s.%s' % (path, options['filename'], file_type(video['url']))
        
        basic_args = ['rtmpdump'] + ['-r', video['url'], '-o', filename]
        
        retval = subprocess.call(basic_args)
        
        while retval == 2 or retval == 1:
            prevsize = os.path.getsize(filename)
            time.sleep(5.0) # This seems to be needed
            retval = subprocess.call(basic_args + ['-e'] + [[], ['-k', '1']][retval == 1])
            cursize = os.path.getsize(filename)
            if prevsize == cursize and retval == 1:
                break
                # Some rtmp streams seem abort after ~ 99.8%. Don't complain for those
            if prevsize == cursize and retval == 2 and cursize > 1024:
                print '\r[rtmpdump] Could not download the whole video.'
                retval = 0
                break
        if retval == 0:
            return True
        else:
            print '\nERROR: rtmpdump exited with code %d' % retval
            return False
        
        return True
    
    print 'Could not find movie file at %s' % url
    return False
    
def parse_url(url):
    return re.search(r'(\d+)', url).groups()[0]

def fetch_options(id):
    
    url = API_URL % id
    
    try:
        response = urlopen(url)    
        data = json.loads(response.read())
    except Exception, e:
        sys.exit(e)
    
    filename = data['statistics']['title']
    videos = data['video']['videoReferences']
    
    alts = []
    for video in videos:
        if video['playerType'] == 'flash':
            alts.append(video)
            
    return {'filename':filename, 'videos':alts}

def main():
    
    if len(sys.argv) > 1:
        if len(sys.argv) > 2:
            download(sys.argv[1], sys.argv[2])
        else:
            download(sys.argv[1])
    else:
        print "please provide video url"

if __name__ == "__main__":
    main()